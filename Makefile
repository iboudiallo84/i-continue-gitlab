
# Parameters
SHELL         = bash
PROJECT       = i-continue-gitlab
GIT_AUTHOR    = COil
HTTP_PORT     = 8000

# Executables
EXEC_PHP      = php
COMPOSER      = composer
REDIS         = redis-cli
GIT           = git
YARN          = yarn
NPX           = npx
PHPUNIT       = ./vendor/bin/simple-phpunit
# Alias
SYMFONY       = $(EXEC_PHP) bin/console
# if you use Docker you can replace "with: docker-composer exec my_php_container $(EXEC_PHP) bin/console"


# Executables: local only
SYMFONY_BIN   = symfony 
DOCKER        = docker
DOCKER_COMP   = docker-compose

CONTAINER_ID_DB = $$(docker container ls -f "name=db_docker_gitlab" -q)
CONTAINER_ID_WW = $$(docker container ls -f "name=www_docker_gitlab" -q)

DB = docker exec -ti $(CONTAINER_ID_DB)
ww = docker exec -ti $(CONTAINER_ID_WW)
QUERY_DB = $(DOCKER) container exec -it $(CONTAINER_ID_WW) 
# Misc
.DEFAULT_GOAL = help
.PHONY       =  # Not needed here, but you can put your all your targets to be sure
                # there is no name conflict between your files and your targets.

## —— 🐝 The Symfony Makefile 🐝 ———————————————————————————————————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

## —— Composer 🧙‍♂️ ————————————————————————————————————————————————————————————
install: composer.lock ## Install vendors according to the current composer.lock file
	$(COMPOSER) install --no-progress --prefer-dist --optimize-autoloader


## —— Symfony 🎵 ———————————————————————————————————————————————————————————————
sf: ## List all Symfony commands
	$(SYMFONY)

cc: ## Clear the cache. DID YOU CLEAR YOUR CACHE????
	$(SYMFONY) c:c

warmup: ## Warmup the cache
	$(SYMFONY) cache:warmup

fix-perms: ## Fix permissions of all var files
	chmod -R 777 var/*

assets: purge ## Install the assets with symlinks in the public folder
	$(SYMFONY) assets:install public/ --symlink --relative

serve: ## Serve the application with HTTPS support
	$(SYMFONY_BIN) serve --daemon --port=$(HTTP_PORT)

unserve: ## Stop the webserver
	$(SYMFONY_BIN) server:stop

## —— Docker 🐳 ————————————————————————————————————————————————————————————————
up: ## Start the docker hub (MySQL,redis,adminer,elasticsearch,head,Kibana)
	$(DOCKER_COMP) up -d

docker-build: ## UP+rebuild the application image
	$(DOCKER_COMP) up -d --build

docker-recreate: ## UP+rebuild the application image
	$(DOCKER_COMP) up --force-recreate --abort-on-container-exit --build --remove-orphans

bash: ## Connect to the application container
	$(DOCKER) container exec -it $(CONTAINER_ID_WW) bash

db-bash: ## Connect to the application container
	$(DOCKER) container exec -it $(CONTAINER_ID_DB) bash

## —— Project 🐝 ———————————————————————————————————————————————————————————————
start: up serve

stop: down unserve ## Stop docker and the Symfony binary server

db-connect:
	$(DB) mysql

db-connect-update:
	$(DOCKER) container exec -it $(CONTAINER_ID_WW) $(SYMFONY) doctrine:database:create --if-not-exists

host-db:
	docker inspect $(CONTAINER_ID_DB) | grep IPAddress

db-drop: ## Drop database
	$(QUERY_DB) $(SYMFONY) doctrine:schema:drop --force
db-validate: ## Build the DB, control the schema validity, load fixtures and check the migration status
	$(QUERY_DB) $(SYMFONY) doctrine:cache:clear-metadata
	# $(QUERY_DB) $(SYMFONY) doctrine:schema:drop --force 
	$(QUERY_DB) $(SYMFONY) doctrine:database:create --if-not-exists
	$(QUERY_DB) $(SYMFONY) doctrine:schema:validate


prepare-dev:
	$(QUERY_DB) $(SYMFONY) d:d:c --if-not-exists  --env=dev
	$(QUERY_DB) $(SYMFONY) d:s:u -f --env=dev

schema-update:
	$(QUERY_DB) $(SYMFONY) d:s:u -f --env=dev

full-db: db-validate prepare-dev

## —— Tests ✅ —————————————————————————————————————————————————————————————————
test: 
	$(PHPUNIT) --stop-on-failure  --testdox
